# APIs

## What is an API

`API` stands for **Application Programming Interface**.

It means a set of functions that an application can call to speak to another application.

API can be used in different contexts:

- The API of a code library is the list of types and functions that a user can use. Examples:
  - The `fetch()` function and the `Request` type are part of the `Fetch API` (https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)
  - The `document.getElementById()` function is part of the API of the `Document` class (https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById)
- An API for a physical device provide a set of functions to control that device. Constructor are responsible of implementing drivers that match APIs. Examples:
  - The OpenGL API (https://www.khronos.org/registry/OpenGL/specs/gl/glspec46.core.pdf) describes a set of functions to control drawing with a GPU (Graphical Processing Unit). Constructors such as NVidia or AMD are responsible for the implementation, but open source drivers may exist.
- The API of a web service is a list of `endpoints` exposed as URLs. These URLs can be accessed and perform actions on the web service, as well as returning data. Examples:
  - The `/2/tweets/search/stream` is part of the Twitter v2 API. It can be accessed with the complete URL `https://api.twitter.com/2/tweets/search/stream`, but require authentication. The full list of endpoints for twitter is here https://developer.twitter.com/en/docs/api-reference-index#twitter-api-v2
  - The `/v2/pokemon/{id or name}/` is part of the Pokemon API. It can be used to query pokemon data using a complete URL. For example `https://pokeapi.co/api/v2/pokemon/pikachu` gives us data about Pikachu. The full documentation is here https://pokeapi.co/docs/v2. This API does not require authentication.
  - The Discord API (https://discord.com/developers/docs/intro) provides a way to control discord: add friends, send messages, build bots.

## REST APIs

`REST APIs` are APIs for web services, like the pokemon API, the twitter API or the discord API described above.

`REST` stands for **representational state transfer**, indicating that we usually use these APIs to transfer a representation of the state of a web service. For example, fetching tweets with the Twitter API gives us a representation of the state of twitter at a given state.

We refer to URLs of a REST API as **endpoints**. So an endpoint is just a URL to talk to a REST API.

Since these APIs are most often exposed with HTTP (with URLs), it means we can do several actions on a given URL:
- GET action: allow to retrieve data.
  - Example: get tweets from twitter, get friends from discord
- POST action: allow to create new data
  - Example: post a new tweet on twitter, send a message to a friend on discord
- PATCH action: allow to change existing data
  - Example: change your name on twitter, change your status on discord
- PUT action: allow to update existing data or create it if it does not exists
  - Example: update the status to "busy" for chat bot "toto" on discord and create it if it does not already exists.

More information about HTTP verbs here: https://blog.cloud-elements.com/http-verbs-demystified-patch-put-and-post

It is quite important to have good knowledge about HTTP to manipulate a REST API.

## The power of REST APIs

Before REST APIs, it was quite difficult to create communication between web services. But now that REST APIs has become a standard, most online web services provide a REST APIs, allowing to create a network of applications interacting together.

For example, a fun project could be:
- Use the pokemon API to create a twitter account for each one
- On backend simulate battles between pokemons
- When a pokemon win against another one, emit a tweet with the account created for him with a taunt about its victory
- Create a discord account for each pokemon
- When someone talks to the pokemon on discord, run a chat bot that just answer with the pokemon name (a pokemon just repeat its name)
- Allow anyone to catch the pokemon on discord by running a battle using discord commands
- When the pokemon is caught, emit a tweet indicating this pokemon has been caught

We see in this example that we can use 3 independant web services to create a new totally different application, which is really powerful.

## Advices for learning how to use APIs

- Be patient, while the way of a calling a REST APIs is the same for all (using http requests on endpoints), each API has its specificities.
- Spend time in the documentation of the API
- Try things in a simple script, create test functions and keep them as reference

## Todo

Here is a list of tasks. Some are just reading things, others are coding. There is no particular order, you can do a bit of each one, read documentation as you code to fill your needs, etc.

Reading:

- Read https://www.wikiwand.com/fr/Interface_de_programmation and write a summary
- Read https://www.wikiwand.com/fr/Hypertext_Transfer_Protocol and write a summary
- Read https://www.wikiwand.com/fr/Representational_state_transfer and write a summary
- Read the (short) book https://drive.google.com/file/d/18tzUevYewNH63dRqbBPZl1KvIPz0l2Uv/view?usp=sharing

Analysis:

- Go to some web pages and open the chrome debug interface. Go to "Network" tab and click on a request. Try to understand the relationship between what is displayed and the HTTP protocol.
- Experiment with pokemon API. For example, access the URLs:
  - https://pokeapi.co/api/v2/ability/9/
  - https://pokeapi.co/api/v2/pokemon/pikachu
  - https://pokeapi.co/api/v2/pokemon/?offset=0&limit=1118
  - Then copy the data in a json file
  - Try to understand the data
  - And use the documentation to better understand

Coding:

- Write a simple web application with HTML, CSS+SASS, Javascript that uses the Pokemon API.
- The user write the name of a pokemon on an input field and click on a search button
- The application display the sprites of the pokemon as well as the list of its abilities and types.
- Display the sprites images
- The user can click on an ability, which should load the description of that ability and display its english description in a paragraph of the page.
- The user can click on a type, which should load the description of that type.
- You can use the example code for inspiration
- What you will need to use in javascript:
  - `fetch()`, see https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API and example
  - `document.*` functions, see https://developer.mozilla.org/en-US/docs/Web/API/Document
  - The `DOM` API: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction

## A final note

REST APIs are progressively replace by GraphQL APIs (Graph Query Language), which are similar but more powerful and allow for better performance.

It would be a good idea to learn GraphQL at some point, but REST APIs remains now the most frequent ones.
