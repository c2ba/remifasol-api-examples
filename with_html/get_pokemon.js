function main() {
  const pikachu = fetch("https://pokeapi.co/api/v2/pokemon/pikachu")
    .then(response => response.json())
    .then(data => {
      document.getElementById("pokemon_front_default").setAttribute("src", data["sprites"]["front_default"])
    })
}

main()
