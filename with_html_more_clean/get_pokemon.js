function set_pokemon_image(data) {
  document.getElementById("mon_image").setAttribute("src", data["sprites"]["front_default"])
}

const set_pokemon_image = data => {
  document.getElementById("mon_image").setAttribute("src", data["sprites"]["front_default"])
}

function main() {
  fetch("https://pokeapi.co/api/v2/pokemon/bulbasaur")
    .then(response => response.json())
    .then(data => {
      set_pokemon_image(data)
    })
}

main()
