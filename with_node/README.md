Demonstrate how to use fetch() to get pokemons from the pokemon API. See https://pokeapi.co/docs/v2#pokemon.

To install local dev end:

```bash
cd with_node
npm install
```

Then run:


```bash
node main.js
```
